# Щоб додати  нове домашнє завдання в новий репозиторій на Gitlab:
> створю новий репозиторій —> створю пусту папку на робочому столі   —> відкрию папку  в WebStorm —> у вікні WS відкрию Термінал  і виконаю такі команди:
* git init
* git remote add origin «посилання на репозиторій»
*  git fetch
* git checkout main
> —> створю файли з ДЗ —>
* git status
* git add .
* git commit -m
* git push origin main
# Щоб додати існуюче домашнє завдання в новий репозиторій на Gitlab:
> Cтворю новий репозиторій на GitLab --> в папці з ДЗ відкрию термінал --> і виконаю такі команди:
* git init
* git remote add origin «посилання на репозиторій»
* git add .
* git commit -m
* git push  origin master
